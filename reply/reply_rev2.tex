\documentclass[12pt]{article}
\linespread{1.2}
%
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[authoryear]{natbib}
\usepackage{enumitem}
\usepackage[margin=1in]{geometry}
\usepackage{mathrsfs}
\usepackage{amsfonts}

%
\newcommand{\vx}{\mathbf{x}}
\newcommand{\vy}{\mathbf{y}}

\title{Maximizing the value of pressure data in saline aquifer characterization}

\author{Seonkyoo Yoon, John R. Williams, Ruben Juanes and Peter K. Kang^*}

\begin{document}

\subsection*{Manuscript ADWR-2017-352\\
Reply to the evaluation and comments of Reviewer~2}

We appreciate the reviewer for the constructive comments towards our manuscript. We have taken the reviewer's comments seriously and have addressed them in our reply and revised manuscript. Below we give detailed responses to all the points raised in the review (the comments are set in italics).

\paragraph{Summary}\textit{Authors investigated the value of pressure data for the saline aquifer characterization. The paper is well-written and suitable for AWR. I have several general comments: }

\paragraph{Response.}
The reviewer states that ``the paper is well-written and suitable for AWR.'' We thank the Reviewer for the positive comments and constructive suggestions for clarification and improvement.

\paragraph{1.}\textit{In terms of methodology, the EnKF has been applied in multiple-dispensaries for a range of parameter characterizations including the permeability using pressure data. The main purpose is to explore the application of the EnKF in varied density flow modeling. For this reason, I would say that the novelty is insufficient for a publication in a journal like AWR.}

\paragraph{Response.}
The main purpose of this study is not the simple application of the EnKF to a saline aquifer, though it is true that this is the first study that applied the EnKF to a density-variant flow and transport problem. The main contribution of this study is the identification of the density effects on the value of pressure data in saline aquifer characterization, and the EnKF is a tool for the inverse modeling. We demonstrate that the information content in pressure data can be maximized at a balanced mixed convection regime, which can be potentially applied at field sites. To elucidate the novelty and originality of this study we have revised the introduction and added relevant references as follows.

\begin{quote}
    \textsf{Line 37: Although many studies have shown the density effects on groundwater flow~\citep{WRCR:WRCR5369, vereecken2000analysis, beinhorn20053, WRCR:WRCR12114, GRL:GRL55795}, the variable-density effect on the value of pressure data has not been systematically studied.}
\end{quote}


\begin{quote}
    \textsf{Line 68: The main contribution of this study is in elucidating the density effects on the value-of-information in pressure data over wide range of mixed convection regimes. To the best of knowledge, this is also the first study applying the EnKF to a density-variant flow and transport problem.}
\end{quote}


\paragraph{2.}\textit{Although the inflation and localization are included in the EnKF, the key parameters such as damping factor and distance are not justified. For example, with the damping factor of 1.01, it does not make much difference for the covariance, in my opinion. Does damping factor have a range of 0 to 1? }

\paragraph{Response.}
The schemes of covariance inflation and localization for EnKF are now recognized as standard strategies to avoid undesirable effects such as ensemble collapse and spurious correlation between distant points. The inflation factor $1.01$ has been a common choice in several studies \citep{hamill2001distance, tong2012assimilating}, and the range for the covariance localization ($r=50 \text{m}$) represents the distance between the two neighborhood measurement points in the horizontal direction. We agree that the discussion on the role of covariance treatments on the inversion results will improve our manuscript. We therefore performed additional analysis and added section 4.2.3 in the manuscript to discuss the role of the covariance inflation and localization on inversion results. See below for details.

\paragraph{3.}\textit{One of main challenge in the EnKF is the computational cost. A comprehensive EnKF including localization would not require a high ensemble size like 300 as listed in the paper, which may imply that the localization and inflation do not play any role for calculating covariances in this work. Clearly, a sensitivity analysis will be helpful.}

\paragraph{Response.}
High computational costs in using EnKF is mainly due to the large number of forward simulations (as many as the ensemble size) which are required in each assimilation window. A common strategy to relieve such a high computational cost is to parallelize the forward simulations which makes the cost independent of the ensemble size. In this work, we parallelized the forward simulation so that a single inversion run takes less than $10$ minutes. To incorporate the reviewer's comment, we have performed a sensitivity analysis to discuss the role of the covariance inflation and localization. However, please note that the main point of this work is the change in the value of the pressure data as a function of mixed convection ratio, and not the application or development of the efficient EnKF using covariance treatments. 

\begin{quote}
	\textsf{Line 497: We also performed a sensitivity analysis to assess the effects of the covariance inflation and localization. Three scenarios of the covariance treatments are considered: 1. no covariance treatments; 2. with inflation ($\omega=1.01$) but no localization; 3. with localization ($r=50 \text{m}$) but no inflation. The estimated error reductions for the three different scenarios are shown in Figure 18. The covariance localization improves the estimation accuracy, whereas the improvements by the covariance inflation are insignificant. This implies that the ensemble size of $300$ is large enough to avoid the ensemble collapse, while circumventing spurious correlations between distant points by the covariance localization improves the estimation accuracy. The results confirm that the estimation accuracy is maximized at the balanced mixed convection regime ($M=1$) regardless of the covariance treatments. }
\end{quote}

\bibliographystyle{plainnat}
\bibliography{reply}

\newpage

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Difficult.png}%
\caption{(\textit{Figure 18 in the updated manuscript}) The reduced error as a function of the mixed convection ratio $M$ for three different scenarios of covariance treatments: 1. no covariance treatments; 2. with covariance inflation ($\omega=1.01$); 3. with covariance localization ($r=50 \text{m}$). The reduced error represents the accuracy of the estimated permeability field. The result shows that the accuracy is maximized at a balanced mixed convection ratio regardless of the covariance treatments.}%
\label{Eperm(Difficult)}%
\end{figure}

\end{document}
