\documentclass[12pt]{article}
\linespread{1.2}
%
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[authoryear]{natbib}
\usepackage{enumitem}
\usepackage[margin=1in]{geometry}
\usepackage{mathrsfs}
\usepackage{amsfonts}

%
\newcommand{\vx}{\mathbf{x}}
\newcommand{\vy}{\mathbf{y}}

\title{Maximizing the value of pressure data in saline aquifer characterization}

\author{Seonkyoo Yoon, John R. Williams, Ruben Juanes and Peter K. Kang^*}

\begin{document}

\subsection*{Manuscript ADWR-2017-352\\
Reply to the evaluation and comments of Reviewer~1}

We appreciate the reviewer for the positive comments towards our manuscript. We have taken the reviewer's comments seriously and have addressed them in our reply and revised manuscript. Below we give detailed responses to all the points raised in the review (the comments are set in italics).

\paragraph{General Comments}\textit{Nice application of the ensemble Kalman filter for aquifer characterization in the presence of freshwater and saltwater. It presents a thorough analysis of the different parameters that control the mixing of fresh and saltwater, and the information content that pressure can carry for the purpose of improving the characterization of a heterogeneous conductivity field. The paper is well written and the results properly discussed.}

\paragraph{Response.}
The reviewer nicely summarizes the key point of our paper and states that ``the paper is well-written and the results properly discussed.'' We thank the Reviewer for the positive comments and constructive suggestions for clarification and improvement. Below we give detailed responses to the two specific points raised in the review.

\paragraph{Specific Comments}
\paragraph{1.}\textit{My only complaint would be that when they talk about ``high heterogeneity'' they only have a variance of $0.5$ for log conductivity, a variability that will never be considered as high heterogeneity. The authors should have analyzed a truly highly heterogeneous field with a variance substantially larger than $1$.}

\paragraph{Response.}
We agree that the variance of $0.5$ is rather of moderate heterogeneity than high heterogeneity. To incorporate the reviewer's comment, we have updated the high heterogeneity section with the variance $1$ case. However, please note that we did conduct the analysis with $\sigma^2_{\ln k}$ up to $3$ and presented the results in Figure 17 of the original manuscript. The impact of heterogeneity on density effects for both Gaussian and Spherical variogram types are shown in Figure \ref{m0m1} (\textit{Figure 15 in the updated manuscript}). We find that the improvements of the information content at a balanced mixed convection regime decrease as heterogeneity increases due to the emergence of preferential flow. We also condensed ``high heterogeneity'' section to improve the readability of the manuscript.

\paragraph{2.}\textit{A second minor comment is the unfortunate choice of a Gaussian variogram to characterize hydraulic conductivity when we all know that a Gaussian variogram will always produce very smooth, unrealistic, fields, which, in turn, are easier to characterize.}

\paragraph{Response.}
Our analysis is not limited to Gaussian variogram as we also performed the analysis with spherical variogram types. We used two different variogram types to show that the \emph{balanced} mixed convection regime maximizes the information content in the pressure data regardless of the types of the spatial correlation structure. We have revised the manuscript to clarify this point as follows:

\begin{quote}
	\textsf{Line 445: We conduct the same analysis for permeability fields with higher degrees of heterogeneity ($\sigma^2_{\ln k}$ up to 3) to determine whether our identification of an optimum mixed convection regime for permeability estimation can be generalized. As a representative case, we first present $\sigma^2_{\ln k}=1$ case. The spatial correlation structure is modeled using a spherical variogram, which generates fields that are less smooth than in a Gaussian variogram model.}

	\textsf{Line 453: The mean and standard deviation of the estimated permeability fields for $M=[0.01, 1, 10]$ and $\beta =1$ are shown in Figure 12. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. We confirmed with the four measures that the value of the pressure data is maximized at the balanced mixed convection regime also for $\sigma^2_{\ln k}=1$ with the spherical variogram case. We present, for brevity, only the error reduction and the the time evolution of the normalized uncertainty measure.}

 	\textsf{Line 470: We also performed the inverse modeling for higher values of heterogeneity ($\sigma^2_{\ln k}$ up to $3$) and found that the inverse estimation is always optimum around $M\approx1$. In Figure 15, we show the differences (or improvements) of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ for different levels of heterogeneity. We clearly observe that the estimation improvement decreases as heterogeneity increases for both Gaussian and spherical variogram cases. This might be explained by the fact that the heterogeneity causes preferential flow that dominates flow behavior \citep{KUNG199051, Fiori2012, kang2016emergence,kang2017anomalous}. This implies that the variable-density effect on flow behavior will decrease as heterogeneity increases.}
\end{quote}

To further generalize our findings, we tested the effects of the monitoring system settings and the covariance treatments. The analysis is added in the section 4.2.3. Please see below for details.

\begin{quote}
	\textsf{Line 482: We provide an additional case study to demonstrate that our findings are also valid for different settings of observation network. We test a moderately heterogeneous case with $\sigma^2_{\ln k}=0.5$ and $E \big[ \ln k \big] = -23$ as shown in Figure 16. There are $16$ measurement points, a third less than the previous case studies. Unlike the previous case studies, the $\log$-permeability values at the measurement points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}

	\textsf{Line 489: Figure 17 shows the mean and standard deviation of the estimated permeability fields. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. The optimality of the balanced mixed convection regime for maximizing the value of the pressure data is also confirmed by the four performance measures, and we only present the error reduction in Figure 18. This confirms the optimality of the balanced mixed convection regime is valid for the case with the limited number of monitoring points.}

	\textsf{Line 497: We also performed a sensitivity analysis to assess the effects of the covariance inflation and localization. Three scenarios of the covariance treatments are considered: 1. no covariance treatments; 2. with inflation ($\omega=1.01$) but no localization; 3. with localization ($r=50 \text{m}$) but no inflation. The estimated error reductions for the three different scenarios are shown in Figure 18. The covariance localization improves the estimation accuracy, whereas the improvements by the covariance inflation are insignificant. This implies that the ensemble size of $300$ is large enough to avoid the ensemble collapse, while circumventing spurious correlations between distant points by the covariance localization improves the estimation accuracy. The results confirm that the estimation accuracy is maximized at the balanced mixed convection regime ($M=1$) regardless of the covariance treatments.}
\end{quote}


\bibliographystyle{plainnat}
\bibliography{reply}

\newpage

\begin{figure}%
\includegraphics[width=\columnwidth]{m0m1.png}%
\caption{(\textit{Figure 15 in the updated manuscript}) Differences, or improvements, of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ as a function of $\sigma^2_{\ln k}$. (a-b) the improvements when the Gaussian variogram is used. (c-d) the improvements when the Spherical variogram is used. The estimation improvement decreases as heterogeneity increases.}%
\label{m0m1}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Difficult.png}%
\caption{(\textit{Figure 16 in the updated manuscript}) The reference (true) $\log$-permeability field and locations of pressure measurements. The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=0.5$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 16 measurement points (a third less than the previous cases). The $\log$-permeability values at these points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}%
\label{refPerm(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Difficult.png}%
\caption{(\textit{Figure 17 in the updated manuscript}) Panel (a): the true permeability field in the moderate heterogeneity case study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Difficult.png}%
\caption{(\textit{Figure 18 in the updated manuscript}) The reduced error as a function of the mixed convection ratio $M$ for three different scenarios of covariance treatments: 1. no covariance treatments; 2. with covariance inflation ($\omega=1.01$); 3. with covariance localization ($r=50 \text{m}$). The reduced error represents the accuracy of the estimated permeability field. The result shows that the accuracy is maximized at a balanced mixed convection ratio regardless of the covariance treatments.}%
\label{Eperm(Difficult)}%
\end{figure}



\end{document}
