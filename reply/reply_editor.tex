\documentclass[12pt]{article}
\linespread{1.2}

%
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[authoryear]{natbib}
\usepackage{enumitem}
\usepackage[margin=1in]{geometry}
\usepackage{mathrsfs}
\usepackage{amsfonts}
%
\newcommand{\vx}{\mathbf{x}}
\newcommand{\vy}{\mathbf{y}}
\title{Maximizing the value of pressure data in saline aquifer characterization}

\author{Seonkyoo Yoon, John R. Williams, Ruben Juanes and Peter K. Kang^*}

\begin{document}

\subsection*{Manuscript ADWR-2017-352\\
Reply to the evaluation and comments from the Editor} 
\vspace{.5cm}
Dear Prof. D'Odorico, 
\vspace{.5cm}

Thank you for the handling of our manuscript and forwarding the referee comments to us. We have carefully considered the comments of the associate editor and all the referees and revised the manuscript accordingly. Below we provide detailed responses to all comments and a description of the changes made in manuscript. We also uploaded the revised version of the manuscript as well as a ``tracked changes'' version of the manuscript.

We hope that our detailed response and the modifications to the manuscript will convince the Editor of the novelty and relevance of our work, and that it is a contribution that belongs in \textit{Advances in Water Resources}. 
\vspace{.5cm}

Sincerely, 

\noindent
Peter K. Kang and co-authors. 

\newpage

\subsection*{Reply to the evaluation and comments of Associate Editor}
We thank the Associate Editor for the constructive comments and suggestions. Below we give detailed responses to all the points raised in the review (the comments are set in italics), and described the key changes made in the manuscript.

\paragraph{}\textit{We received now three reviews for your paper "Maximizing the value of pressure data in saline aquifer characterization". The reviewers consider in general that your paper is interesting and well written. The three reviewers recommend major revision, minor revision and acceptance of the work. As a summary, the main points to be handled are in my opinion:
1. The novelty of the work should be better explained in the introduction of the paper in relation to the already large body of existing literature.}

\paragraph{\textit{Response.}}
Reviewer 2 questioned the novelty of this study by describing our work as the application of the EnKF to a varied density flow problem. However, the main purpose of this study is not the simple application of the EnKF to a saline aquifer, though it is true that this is the first study that applied the EnKF to a density-variant flow and transport problem. The main contribution of this study is the identification of the density effects on the value of pressure data in saline aquifer characterization, and the EnKF is a tool for the inverse modeling. We demonstrate that the information content in pressure data can be maximized at a balanced mixed convection regime, which can be potentially applied at field sites. These contributions are acknowledged by the reviewer 1 and 3. To better elucidate the novelty and originality of this study we have revised the introduction and added relevant references as follows.

\begin{quote}
    \textsf{Line 37: Although many studies have shown the density effects on groundwater flow~\citep{WRCR:WRCR5369, vereecken2000analysis, beinhorn20053, WRCR:WRCR12114, GRL:GRL55795}, the variable-density effect on the value of pressure data has not been systematically studied.}
\end{quote}


\begin{quote}
    \textsf{Line 68: The main contribution of this study is in elucidating the density effects on the value-of-information in pressure data over wide range of mixed convection regimes. To the best of knowledge, this is also the first study applying the EnKF to a density-variant flow and transport problem.}
\end{quote}

\paragraph{}\textit{2. The results from experiment~2 do not provide much additional insight according reviewer~3. This reviewer gives some suggestions for additional, different simulation experiments, for example with erroneous hydraulic conductivity statistics. I think that here also comments from other reviewers can add in and a test could be performed with a stronger heterogeneity of hydraulic conductivity (reviewer 1), and a more detailed exploration of the role of localization and inflation (reviewer 2). I think that additional calculations can strengthen the paper and recommend the editor moderate revision of the paper.}

\paragraph{\textit{Response.}}
The points we are trying to make with the second experiment are: 1. the balanced mixed convection regime maximizes the information content in the pressure data regardless of the heterogeneity and variogram types; 2. although the optimality is still valid for high heterogeneity cases, the improvements of the information content by enforcing a balanced mixed convection regime decreases as the heterogeneity increases due to the emergence of preferential flow. We revised the manuscript to elucidate these points. Also, to incorporate the reviewer 1's comment, the second experiment is replaced with the variance $1$ case and the Figure~17 in the original manuscript is updated to clearly show the results for the variance upto $3$ (\textit{Figure 15 in the updated manuscript}). We also condensed ``high heterogeneity'' section to improve the readability of the manuscript. Please see below for details. 

\begin{quote}
	\textsf{Line 445: We conduct the same analysis for permeability fields with higher degrees of heterogeneity ($\sigma^2_{\ln k}$ up to 3) to determine whether our identification of an optimum mixed convection regime for permeability estimation can be generalized. As a representative case, we first present $\sigma^2_{\ln k}=1$ case. The spatial correlation structure is modeled using a spherical variogram, which generates fields that are less smooth than in a Gaussian variogram model.}

	\textsf{Line 453: The mean and standard deviation of the estimated permeability fields for $M=[0.01, 1, 10]$ and $\beta =1$ are shown in Figure 12. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. We confirmed with the four measures that the value of the pressure data is maximized at the balanced mixed convection regime also for $\sigma^2_{\ln k}=1$ with the spherical variogram case. We present, for brevity, only the error reduction and the the time evolution of the normalized uncertainty measure.}

 	\textsf{Line 470: We also performed the inverse modeling for higher values of heterogeneity ($\sigma^2_{\ln k}$ up to $3$) and found that the inverse estimation is always optimum around $M\approx1$. In Figure 15, we show the differences (or improvements) of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ for different levels of heterogeneity. We clearly observe that the estimation improvement decreases as heterogeneity increases for both Gaussian and spherical variogram cases. This might be explained by the fact that the heterogeneity causes preferential flow that dominates flow behavior \citep{KUNG199051, Fiori2012, kang2016emergence,kang2017anomalous}. This implies that the variable-density effect on flow behavior will decrease as heterogeneity increases.}
\end{quote}

In this paper, we assume that the $\ln \mathbf{k}$ is multi-Gaussian with pre-known spatial statistics such as the mean and covariance. Analyzing the effects of such \textit{a priori} knowledge on the inverse estimation is beyond the scope of this work. However, we agree that such \textit{a priori} knowledge is not always available at field sites and should be clarified in the manuscript. We revised the manuscript as follows.

\begin{quote}
	\textsf{The variance of the log-permeability is $(\sigma^2_{\ln k}=0.25)$, which is similar to those of low-heterogeneity natural geological formations such as Borden, Ontario $(\sigma^2_{\ln k}=0.29)$ and Cape Cod, Massachusetts$(\sigma^2_{\ln k}=0.24)$ \citep{mackay1986natural, WRCR:WRCR5370, WRCR:WRCR5791}. Note that we assume that the $\ln \mathbf{k}$ is multi-Gaussian with pre-known spatial statistics such as the mean and covariance.}
\end{quote}

Finally, to further generalize our findings, we performed additional analysis to study the effects of the monitoring system settings and the covariance treatments (role of localization and inflation). The analysis is added in the section 4.2.3. Please see below for details.

\begin{quote}
	\textsf{Line 482: We provide an additional case study to demonstrate that our findings are also valid for different settings of observation network. We test a moderately heterogeneous case with $\sigma^2_{\ln k}=0.5$ and $E \big[ \ln k \big] = -23$ as shown in Figure 16. There are $16$ measurement points, a third less than the previous case studies. Unlike the previous case studies, the $\log$-permeability values at the measurement points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}

	\textsf{Line 489: Figure 17 shows the mean and standard deviation of the estimated permeability fields. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. The optimality of the balanced mixed convection regime for maximizing the value of the pressure data is also confirmed by the four performance measures, and we only present the error reduction in Figure 18. This confirms the optimality of the balanced mixed convection regime is valid for the case with the limited number of monitoring points.}

	\textsf{Line 497: We also performed a sensitivity analysis to assess the effects of the covariance inflation and localization. Three scenarios of the covariance treatments are considered: 1. no covariance treatments; 2. with inflation ($\omega=1.01$) but no localization; 3. with localization ($r=50 \text{m}$) but no inflation. The estimated error reductions for the three different scenarios are shown in Figure 18. The covariance localization improves the estimation accuracy, whereas the improvements by the covariance inflation are insignificant. This implies that the ensemble size of $300$ is large enough to avoid the ensemble collapse, while circumventing spurious correlations between distant points by the covariance localization improves the estimation accuracy. The results confirm that the estimation accuracy is maximized at the balanced mixed convection regime ($M=1$) regardless of the covariance treatments.}
\end{quote}

\bibliographystyle{plainnat}
\bibliography{reply}

\newpage

\begin{figure}%
\includegraphics[width=\columnwidth]{m0m1.png}%
\caption{(\textit{Figure 15 in the updated manuscript}) Differences, or improvements, of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ as a function of $\sigma^2_{\ln k}$. (a-b) the improvements when the Gaussian variogram is used. (c-d) the improvements when the Spherical variogram is used. The estimation improvement decreases as heterogeneity increases.}%
\label{m0m1}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Difficult.png}%
\caption{(\textit{Figure 16 in the updated manuscript}) The reference (true) $\log$-permeability field and locations of pressure measurements. The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=0.5$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 16 measurement points (a third less than the previous cases). The $\log$-permeability values at these points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}%
\label{refPerm(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Difficult.png}%
\caption{(\textit{Figure 17 in the updated manuscript}) Panel (a): the true permeability field in the moderate heterogeneity case study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Difficult.png}%
\caption{(\textit{Figure 18 in the updated manuscript}) The reduced error as a function of the mixed convection ratio $M$ for three different scenarios of covariance treatments: 1. no covariance treatments; 2. with covariance inflation ($\omega=1.01$); 3. with covariance localization ($r=50 \text{m}$). The reduced error represents the accuracy of the estimated permeability field. The result shows that the accuracy is maximized at a balanced mixed convection ratio regardless of the covariance treatments.}%
\label{Eperm(Difficult)}%
\end{figure}

\end{document}


\end{document}
 