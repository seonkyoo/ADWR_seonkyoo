\documentclass[12pt]{article}
\linespread{1.2}
%
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[authoryear]{natbib}
\usepackage{enumitem}
\usepackage[margin=1in]{geometry}
\usepackage{mathrsfs}
\usepackage{amsfonts}

%
\newcommand{\vx}{\mathbf{x}}
\newcommand{\vy}{\mathbf{y}}

\title{Maximizing the value of pressure data in saline aquifer characterization}

\author{Seonkyoo Yoon, John R. Williams, Ruben Juanes and Peter K. Kang^*}

\begin{document}

\subsection*{Manuscript ADWR-2017-352\\
Reply to the evaluation and comments of Reviewer~3}

We appreciate the reviewer for the positive comments towards our manuscript. We have taken the reviewer's comments seriously and have addressed them in our reply and revised manuscript. Below we give detailed responses to all the points raised in the review (the comments are set in italics).

\paragraph{Summary}\textit{The manuscript presents a synthetic study in which permeability fields are estimated for variable density groundwater flow. The studied scenario consists of injected fresh water into a heavily monitored saline aquifer. Pressure data is observed and the usefulness of the data when using different injection rates is evaluated. The authors show that best results can be achieved when considering an injection rate corresponding to a mixed convection ratio of 1.}

\paragraph{Response.}
The reviewer nicely summarizes the key points of our paper.

\paragraph{General Comments}\textit{The manuscript is well written and very pleasant to read. The structure is good and the findings are evaluated with suitable statistics. The setup is rather artificial, especially considering the assumptions about observation density and system knowledge, but for the question addressed it is suitable.}

\paragraph{Response.}
The reviewer states that ``The manuscript is well written and very pleasant to read,'' and ``The structure is good and the findings are evaluated with suitable statistics.'' We thank the Reviewer for the positive comments. We performed additional analysis and updated the manuscript to address the reviewer's comments on the observation density and system knowledge. Please refer to the responses below for details. Below we give detailed responses to the two specific points raised in the review.

\paragraph{Specific comments \\ \underline{1. Observations of permeability}}\textit{On lines 935-396 one reads that the permeability at the observation points are assumed known and equal to the mean of the field. I really do not understand that this in practice means. Are the lnk-observations used in the filter? (The resulting figures really do not look like it). Or used to constrain the initial sample? Or not used at all? Please clarify!}

\paragraph{Response.}
We assumed the log-permeability values at the measurement points are known. The values of the log-permeability at the measurement points are used by enforcing the initial ensemble state vectors to have the known values at the corresponding well points. The enforcement is carried out by the conditional sequential Gaussian simulation \citep{remy2009applied}. Such a construction of the permeability fields with conditional data at measurement points has been commonly used assuming laboratory or in-situ permeability measurements are available \citep{WRCR:WRCR3341, zhou2011approach, WRCR:WRCR21395}. For clarification, we updated the manuscript as follows:

\begin{quote}
	\textsf{Line 364: There are $24$ measurement points, whose $\log$-permeability values are assumed to be known and fixed to the mean value of $E \big[ \ln k \big] = -23$. The assumption of the known values of the log-permeability at the measurement points is applied by enforcing the initial ensemble state vectors to have the known value at the corresponding well points. The enforcement is carried out by the conditional sequential Gaussian simulation \citep{remy2009applied}. Such a construction of the permeability fields with conditional data at measurement points has been used assuming laboratory or in-situ permeability measurements are available \citep{WRCR:WRCR3341, zhou2011approach, WRCR:WRCR21395}. The spatial correlation structure is modeled by a Gaussian variogram with a range of $30\text{ m}$ in the direction $20^ \circ$ from the x-axis and $15\text{ m}$ in the direction $110^\circ$ from x-axis. The same statistics are used to generate $300$ initial permeability ensemble members.}
\end{quote}

Furthermore, we have performed an additional analysis to confirm that our findings are also valid in different settings of the monitoring system such as the observation density and the availability of the \textit{a priori} data of permeability at the monitoring points. The analysis is added in the section 4.2.3, where we also discuss the effects of the covariance inflation and localization. Please see below for details.

\begin{quote}
	\textsf{Line 482: We provide an additional case study to demonstrate that our findings are also valid for different settings of observation network. We test a moderately heterogeneous case with $\sigma^2_{\ln k}=0.5$ and $E \big[ \ln k \big] = -23$ as shown in Figure 16. There are $16$ measurement points, a third less than the previous case studies. Unlike the previous case studies, the $\log$-permeability values at the measurement points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}

	\textsf{Line 489: Figure 17 shows the mean and standard deviation of the estimated permeability fields. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. The optimality of the balanced mixed convection regime for maximizing the value of the pressure data is also confirmed by the four performance measures, and we only present the error reduction in Figure 18. This confirms the optimality of the balanced mixed convection regime is valid for the case with the limited number of monitoring points.}

	\textsf{Line 497: We also performed a sensitivity analysis to assess the effects of the covariance inflation and localization. Three scenarios of the covariance treatments are considered: 1. no covariance treatments; 2. with inflation ($\omega=1.01$) but no localization; 3. with localization ($r=50 \text{m}$) but no inflation. The estimated error reductions for the three different scenarios are shown in Figure 18. The covariance localization improves the estimation accuracy, whereas the improvements by the covariance inflation are insignificant. This implies that the ensemble size of $300$ is large enough to avoid the ensemble collapse, while circumventing spurious correlations between distant points by the covariance localization improves the estimation accuracy. The results confirm that the estimation accuracy is maximized at the balanced mixed convection regime ($M=1$) regardless of the covariance treatments.}
\end{quote}

\paragraph{\underline{2. Experiment 2}}\textit{Since the resulting figures from experiment 2 looks, generally speaking, the same as those for experiment 1, I found this section rather boring to read. My suggestion would be to either move this to supplementary material/appendix or expand it in a more systematic way using the experiments mentions on lines 1185 - (like in Figure 17). As it is now it reads a bit like it is just another random synthetic case which happens to also generate good result, and that is less optimal then what it could be.
It is also not entirely clear what point the authors are trying to make with the second experiment: that the estimation works also with less Gaussian fields? However, moving in this direction, also other strong (even stronger) assumptions effecting the parameter estimation should be discussed up front (e.g. the perfectly known model apart from the K-field, the assumption of perfectly know spatial statistics of the ln-k field, the high observation density)
}

\paragraph{Response.}
We appreciate the reviewer's constructive comments. The points we are trying to make with the second experiment are: 1. the balanced mixed convection regime maximizes the information content in the pressure data regardless of the heterogeneity and variogram types; 2. although the optimality is still valid for high heterogeneity cases, the improvements of the information content by enforcing a balanced mixed convection regime decreases as the heterogeneity increases due to the emergence of preferential flow. We revised the manuscript to elucidate these points. In addition, considering that the variance of 0.5 is rather of moderate heterogeneity, the second experiment is replaced with the variance $1$ case. We also condensed ``high heterogeneity'' section to improve the readability of the manuscript. Please see below for details. 

\begin{quote}
	\textsf{Line 445: We conduct the same analysis for permeability fields with higher degrees of heterogeneity ($\sigma^2_{\ln k}$ up to 3) to determine whether our identification of an optimum mixed convection regime for permeability estimation can be generalized. As a representative case, we first present $\sigma^2_{\ln k}=1$ case. The spatial correlation structure is modeled using a spherical variogram, which generates fields that are less smooth than in a Gaussian variogram model.}

	\textsf{Line 453: The mean and standard deviation of the estimated permeability fields for $M=[0.01, 1, 10]$ and $\beta =1$ are shown in Figure 12. Qualitatively, the $M=1$ case again has the most accurately estimated permeability field with the least uncertainty. We confirmed with the four measures that the value of the pressure data is maximized at the balanced mixed convection regime also for $\sigma^2_{\ln k}=1$ with the spherical variogram case. We present, for brevity, only the error reduction and the the time evolution of the normalized uncertainty measure.}

 	\textsf{Line 470: We also performed the inverse modeling for higher values of heterogeneity ($\sigma^2_{\ln k}$ up to $3$) and found that the inverse estimation is always optimum around $M\approx1$. In Figure 15, we show the differences (or improvements) of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ for different levels of heterogeneity. We clearly observe that the estimation improvement decreases as heterogeneity increases for both Gaussian and spherical variogram cases. This might be explained by the fact that the heterogeneity causes preferential flow that dominates flow behavior \citep{KUNG199051, Fiori2012, kang2016emergence,kang2017anomalous}. This implies that the variable-density effect on flow behavior will decrease as heterogeneity increases.}
\end{quote}

In this paper, we assume that the $\ln \mathbf{k}$ is multi-Gaussian with pre-known spatial statistics such as the mean and covariance. Analyzing the effects of such \textit{a priori} knowledge on the inverse estimation is beyond the scope of this work. However, we agree that such \textit{a priori} knowledge is not always available at field sites and should be clarified in the manuscript. We revised the manuscript as follows.

\begin{quote}
	\textsf{The variance of the log-permeability is $(\sigma^2_{\ln k}=0.25)$, which is similar to those of low-heterogeneity natural geological formations such as Borden, Ontario $(\sigma^2_{\ln k}=0.29)$ and Cape Cod, Massachusetts$(\sigma^2_{\ln k}=0.24)$ \citep{mackay1986natural, WRCR:WRCR5370, WRCR:WRCR5791}. Note that we assume that the $\ln \mathbf{k}$ is multi-Gaussian with pre-known spatial statistics such as the mean and covariance.}
\end{quote}

Also, as we addressed in the response to the specific comment 1, we performed an additional analysis to confirm that our findings are also valid for different settings of the monitoring system such as smaller observation density and the unavailability of the \textit{a priori} permeability data at the well locations. 

\bibliographystyle{plainnat}
\bibliography{reply}


\newpage

\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Difficult.png}%
\caption{(\textit{Figure 16 in the updated manuscript}) The reference (true) $\log$-permeability field and locations of pressure measurements. The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=0.5$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 16 measurement points (a third less than the previous cases). The $\log$-permeability values at these points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}%
\label{refPerm(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Difficult.png}%
\caption{(\textit{Figure 17 in the updated manuscript}) Panel (a): the true permeability field in the moderate heterogeneity case study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Difficult.png}%
\caption{(\textit{Figure 18 in the updated manuscript}) The reduced error as a function of the mixed convection ratio $M$ for three different scenarios of covariance treatments: 1. no covariance treatments; 2. with covariance inflation ($\omega=1.01$); 3. with covariance localization ($r=50 \text{m}$). The reduced error represents the accuracy of the estimated permeability field. The result shows that the accuracy is maximized at a balanced mixed convection ratio regardless of the covariance treatments.}%
\label{Eperm(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{m0m1.png}%
\caption{(\textit{Figure 15 in the updated manuscript}) Differences, or improvements, of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ as a function of $\sigma^2_{\ln k}$. (a-b) the improvements when the Gaussian variogram is used. (c-d) the improvements when the Spherical variogram is used. The estimation improvement decreases as heterogeneity increases.}%
\label{m0m1}%
\end{figure}

\end{document}
