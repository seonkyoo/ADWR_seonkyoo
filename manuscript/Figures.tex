\begin{figure}%
\includegraphics[width=\columnwidth]{henryDomain_Injection.png}%
\caption{Schematic illustration of the confined saline aquifer domain with boundary and initial conditions; this represents a typical coastal aquifer system. Freshwater is injected at the left boundary, where we impose a constant-flux boundary condition; a seawater hydrostatic boundary condition is imposed at the right boundary.}%
\label{henryDomain(Injection)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{enkfChart.png}%
\caption{Flow chart showing data assimilation via the ensemble Kalman filter.}%
\label{enkfChart}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Tilting.png}%
\caption{Flow and transport simulation results for different values of the mixed convection ratio $M$. The $16$ upper panels show the evolution of the injected freshwater plume for different pore volumes injected of freshwater (PVI); the colormap indicates the salinity of the water. The lower four panels show the normalized pressure measurements as a function of PVI (which is a proxy for time) at three different observation locations; these locations are indicated on the upper panels. The pressure values are normalized by the initial pressure value, and the variation of the normalized pressure value is largest at $M=1$. The bottom panels also include, as insets, the breakthrough curves (concentration as a function of PVI) at each of the three wells.}%
\label{Tilting}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Gaussian.png}%
\caption{The reference (true) $\log$-permeability field (top panel) and locations of pressure measurements in the low heterogeneity case study (bottom panel). The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=0.25$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 24 measurement points, whose $\log$-permeability values are assumed to be known and equal to the mean value $E \big[ \ln k \big] = -23$. The spatial correlation structure is modeled by a Gaussian variogram.}.%
\label{refPerm(Gaussian)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{henryDomain_Production.png}%
\caption{Conceptual model for transport predictability tests showing initial and boundary conditions. The case study is constructed to simulate a seawater intrusion scenario. We impose a seawater hydrostatic pressure boundary condition at the right boundary, and freshwater is produced at a constant rate at the left boundary.}%
\label{henryDomain(Production)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Gaussian.png}%
\caption{Panel (a): the true permeability field in the low heterogeneity case study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Gaussian)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Gaussian.png}%
\caption{The reduced error as a function of the mixed convection ratio $M$ for three different values of $\beta$ in the low heterogeneity case study. The reduced error is defined as $\frac{e^{\text{initial}}-e}{e^{\text{initial}}}$, where $e^{\text{initial}}$ is the error of the initial permeability ensemble. This measure represents the accuracy of the estimated permeability field, which is maximized at the balanced mixed convection ratio of one.}% 
\label{Eperm(Gaussian)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{mapAccu_Gaussian.png}%
\caption{The mapping accuracy of the finally updated ensemble mean of $\log$-permeability fields in the low heterogeneity case study as a function of the mixed convection ratio $M$ for three different criteria ($10\%, 15\%, 20\%$). The mapping accuracy is the fraction of correctly estimated grid cells with regard to the true permeability field. A $\log$-permeability estimate of a grid cell is counted correct when the difference between the true and estimated $\log$-permeability values is less than a certain threshold.}%
\label{mapAccu(Gaussian)}%
\end{figure}


\begin{figure}%
\includegraphics[width=\columnwidth]{uncertainty_Gaussian.png}%
\caption{The time evolution of the normalized uncertainty measure, $\frac{\Lambda_{t}}{\Lambda_{0}}$, for $M = [0, 0.01, 1, 10]$ and $\beta = [0.2,1, 5]$ as a function of pore volume injected (PVI) for the low heterogeneity case study. Ten data assimilation steps are conducted for each case, and the largest uncertainty reduction is for the balanced mixed convection ratio of one.}%
\label{uncertainty(Gaussian)}%
\end{figure}



\begin{figure}%
\includegraphics[width=\columnwidth]{Predict_Gaussian.png}%
\caption{The results of the transport predictability tests for three different values of $\beta$ in the low heterogeneity case study. The arrival time mismatch, $\frac{| t_{\text{true}}-t_{\text{predict}} |}{t_{\text{true}}}$, is minimized at a balanced mixed convection ratio of one.  The results are consistent for arrival times of four different concentration values: 2.5\%, 5\%, 10\%, and 20\% of the seawater concentration.}%
\label{Predict(Gaussian)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Spherical.png}%
\caption{The reference (true) $\log$-permeability field and locations of pressure measurements in the high heterogeneity case study. The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=1$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 24 measurement points whose $\log$-permeability values are assumed to be known and equal to the mean value $E \big[ \ln k \big] = -23$. The spatial correlation structure is modeled using a spherical variogram.}%
\label{refPerm(Spherical)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Spherical.png}%
\caption{Panel (a): the true permeability field in the high heterogeneity case ($\sigma^2_{\ln k}=1$) study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Spherical)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Spherical.png}%
\caption{The reduced error as a function of the mixed convection ratio $M$ for three different values of $\beta$ in the high heterogeneity case ($\sigma^2_{\ln k}=1$) study. The reduced error is defined as $\frac{e^{\text{initial}}-e}{e^{\text{initial}}}$, where $e^{\text{initial}}$ is the error of the initial permeability ensemble. This represents the accuracy of the estimated permeability field, which is maximized at a balanced mixed convection ratio of one.}%
\label{Eperm(Spherical)}%
\end{figure}


\begin{figure}%
\includegraphics[width=\columnwidth]{uncertainty_Spherical.png}%
\caption{The time evolution of the normalized uncertainty measure, $\frac{\Lambda_{t}}{\Lambda_{0}}$, for $M = [0, 0.01, 1, 10]$ and $\beta = [0.2,1, 5]$ as a function of pore volume injected (PVI) for the high heterogeneity case ($\sigma^2_{\ln k}=1$) study.  Ten data assimilation steps are conducted for each case, and the largest uncertainty reduction is for the balanced mixed convection ratio of one.}%
\label{uncertainty(Spherical)}%
\end{figure}


\begin{figure}%
\includegraphics[width=\columnwidth]{m0m1.png}%
\caption{Differences, or improvements, of the error reduction and the estimation uncertainty between the optimum mixed convection regime $(M=1)$ and the density-invariant case $(M=0)$ as a function of $\sigma^2_{\ln k}$. (a-b) the improvements when the Gaussian variogram is used. (c-d) the improvements when the Spherical variogram is used. The estimation improvement decreases as heterogeneity increases.}%
\label{m0m1}%
\end{figure}


\begin{figure}%
\includegraphics[width=\columnwidth]{refPerm_Difficult.png}%
\caption{The reference (true) $\log$-permeability field and locations of pressure measurements. The $\log$-permeability field is assumed to follow a Gaussian process with a log-permeability mean $E \big[ \ln k \big] = -23$ and a log-permeability variance $\sigma^2_{\ln k}=0.5$. The field is defined on a $200 \times 50$ grid with cells of size $1 \text{ m} \times 1 \text{ m}$. There are 16 measurement points (a third less than the previous cases). The $\log$-permeability values at these points are assumed \emph{unknown}. The spatial correlation structure is modeled using a spherical variogram.}%
\label{refPerm(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{permUpdates_Difficult.png}%
\caption{Panel (a): the true permeability field in the moderate heterogeneity case study. Panels (b-d): mean of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (b), $M=1$ (c), and $M=10$ (d). Panel (e): standard deviation of the initial ensemble of permeability fields. Panels (f-h): standard deviation of the estimated permeability fields after the final update for the cases with $\beta = 1$ and $M=0.01$ (f), $M=1$ (g), and $M=10$ (h).}%
\label{permUpdates(Difficult)}%
\end{figure}

\begin{figure}%
\includegraphics[width=\columnwidth]{Eperm_Difficult.png}%
\caption{The reduced error as a function of the mixed convection ratio $M$ for three different scenarios of covariance treatments: 1. no covariance treatments; 2. with covariance inflation ($\omega=1.01$); 3. with covariance localization ($r=50 \text{m}$). The reduced error represents the accuracy of the estimated permeability field. The result shows that the accuracy is maximized at a balanced mixed convection ratio regardless of the covariance treatments.}%
\label{Eperm(Difficult)}%
\end{figure}